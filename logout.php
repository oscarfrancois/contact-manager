<?php

/**
 * Page déconnectant l'utilisateur puis le redirigeant vers la page d'accueil.
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date volée 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

session_start();

define("ROOT", ".");

/**
 * suppression du cookie de session, des variabes de session et destruction de la session
 */
function logout()
{
    // destruction du cookie de session
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(
            session_name(),
            '',
            time() - 42000,
            $params["path"],
            $params["domain"],
            $params["secure"],
            $params["httponly"]
        );
    }

    // suppression des variables de session
    $_SESSION = array();

    // destruction de la session
    session_destroy();
}

logout();

// comme la session est supprimée, security.php redirigera vers la page par défaut
require_once(ROOT . "/lib/security.php");
