<?php

/**
 * Suppression d'un contact
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date volée 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

session_start();

define("ROOT", "..");
require_once(ROOT . "/lib/security.php");
require_once(ROOT . "/lib/database/database.php");

// si l'id du contact n'est pas valable, on retourne à la liste des contacts
if (!isset($_GET["id"]) || empty($_GET["id"])) {
    header("Location: index.php");
}

// suppression du contact 
$id = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
$contact = deleteContact($id);
header("Location: index.php");
