<?php

/**
 * Mise à jour des informations d'un contact
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date volée 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

session_start();

define("ROOT", "..");
require_once(ROOT . "/lib/security.php");
require_once(ROOT . "/lib/database/database.php");

// si l'id du contact n'est pas valable, on retourne à la liste des contacts
if (!isset($_GET["id"]) || empty($_GET["id"])) {
    header("Location: index.php");
}

// traitement du formulaire
if (
    isset($_POST["name"]) && !empty($_POST["name"]) &&
    isset($_POST["surname"]) && !empty($_POST["surname"]) &&
    isset($_POST["phone"]) && !empty($_POST["phone"]) &&
    isset($_POST["email"]) && !empty($_POST["email"]) &&
    isset($_POST["id"]) && !empty($_POST["id"])
) {
    $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_SPECIAL_CHARS);
    $surname = filter_input(INPUT_POST, "surname", FILTER_SANITIZE_SPECIAL_CHARS);
    $phone = filter_input(INPUT_POST, "phone", FILTER_SANITIZE_SPECIAL_CHARS);
    $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_SPECIAL_CHARS);
    $id = filter_input(INPUT_POST, "id", FILTER_VALIDATE_INT);

    updateContact($name, $surname, $phone, $email, $id);
    header("Location: index.php");
}

// récupération des détails du contact 
$id = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
$contact = getContact($id);

?><h1>Modification d'un contact</h1>

<form method="post" action="">
    <input type="hidden" id="id" name="id" value="<?= $contact["id"] ?>">
    <table>
        <tr>
            <td><label for="name">Prénom: </label></td>
            <td><input type="text" id="name" name="name" value="<?= $contact["name"] ?>" autofocus></td>
        </tr>
        <tr>
            <td><label for="surname">Nom: </label></td>
            <td><input type="text" id="surname" name="surname" value="<?= $contact["surname"] ?>"></td>
        </tr>
        <tr>
            <td><label for="phone">Téléphone: </label></td>
            <td><input type="text" id="phone" name="phone" value="<?= $contact["phone"] ?>"></td>
        </tr>
        <tr>
            <td><label for="email">Courriel: </label></td>
            <td><input type="email" id="email" name="email" value="<?= $contact["email"] ?>"></td>
        </tr>
    </table>
    <p><input type="submit" value="Modifier"></p>
</form>

<p><a href="index.php">Retour à la liste des contacts</a></p>