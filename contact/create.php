<?php

/**
 * Création d'un contact pour l'utilisateur connecté
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date volée 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

session_start();

define("ROOT", "..");
require_once(ROOT . "/lib/security.php");
require_once(ROOT . "/lib/database/database.php");

// traitement du formulaire
if (
    isset($_POST["name"]) && !empty($_POST["name"]) &&
    isset($_POST["surname"]) && !empty($_POST["surname"]) &&
    isset($_POST["phone"]) && !empty($_POST["phone"]) &&
    isset($_POST["email"]) && !empty($_POST["email"])
) {
    $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_SPECIAL_CHARS);
    $surname = filter_input(INPUT_POST, "surname", FILTER_SANITIZE_SPECIAL_CHARS);
    $phone = filter_input(INPUT_POST, "phone", FILTER_SANITIZE_SPECIAL_CHARS);
    $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_SPECIAL_CHARS);

    $idContact  = createContact($name, $surname, $phone, $email, $_SESSION["idUser"]);
    header("Location: read.php?id=$idContact");
}

?><h1>Ajouter un contact</h1>


<form method="post" action="">
    <table>
        <tr>
            <td><label for="name">Prénom: </label></td>
            <td><input type="text" id="name" name="name" autofocus></td>
        </tr>
        <tr>
            <td><label for="surname">Nom: </label></td>
            <td><input type="text" id="surname" name="surname"></td>
        </tr>
        <tr>
            <td><label for="phone">Téléphone: </label></td>
            <td><input type="text" id="phone" name="phone"></td>
        </tr>
        <tr>
            <td><label for="email">Courriel: </label></td>
            <td><input type="email" id="email" name="email"></td>
        </tr>
    </table>
    <p><input type="submit" value="Ajouter"></p>
</form>
<p><a href="index.php">Retour à la liste des contacts</a></p>