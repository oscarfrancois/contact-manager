<?php

/**
 * Page affichant les contacts de l'utilisateur connecté
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date volée 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

session_start();

define("ROOT", "..");
require_once(ROOT . "/lib/security.php");
require_once(ROOT . "/lib/database/database.php");
?>

<h1> Liste des contacts </h1>

<p><a href="<?= ROOT . "/contact/create.php" ?>">Ajouter un contact</a></p>

<table border="1">
    <tr>
        <th>Prénom</th>
        <th>Nom</th>
        <th>Téléphone</th>
        <th>Courriel</th>
        <th>Action</th>
    </tr>
    <?php

    $contacts = getContacts($_SESSION["idUser"]);
    foreach ($contacts as $contact) {
        echo "<tr>";
        echo "<td>" . $contact["name"] . "</td>";
        echo "<td>" . $contact["surname"] . "</td>";
        echo "<td>" . $contact["phone"] . "</td>";
        echo "<td>" . $contact["email"] . "</td>";
        echo "<td>" .
            " <a href='" . ROOT . "/contact/read.php?id=" . $contact["id"] . "'>Détails</a>" .
            " <a href='" . ROOT . "/contact/update.php?id=" . $contact["id"] . "'>Modifier</a>" .
            " <a href='" . ROOT . "/contact/delete.php?id=" . $contact["id"] . "'>Effacer</a>" .
            "</td>";
        echo "</tr>";
    }
    ?>
</table>

<form method="post" action="<?= ROOT . "/logout.php" ?>">
    <input type="submit" value="Se déconnecter">
</form>