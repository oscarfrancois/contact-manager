<?php

/**
 * Page affichant les informations détaillées d'un contact
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date volée 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

session_start();

define("ROOT", "..");
require_once(ROOT . "/lib/security.php");
require_once(ROOT . "/lib/database/database.php");

// si l'id du contact n'est pas valable, on retourne à la liste des contacts
if (!isset($_GET["id"]) || empty($_GET["id"])) {
    header("Location: index.php");
}

// récupération des détails du contact 
$id = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
$contact = getContact($id);
?>

<h1> Détail du contact </h1>

<table border="1">
    <tr>
        <th>Prénom</th>
        <th>Nom</th>
        <th>Téléphone</th>
        <th>Courriel</th>
    </tr>
    <tr>
        <td><?= $contact["name"] ?></td>
        <td><?= $contact["surname"] ?></td>
        <td><?= $contact["phone"] ?></td>
        <td><?= $contact["email"] ?></td>
    </tr>
</table>

<p><a href="index.php">Retour à la liste des contacts</a></p>