<?php

/**
 * Page d'accueil vérifiant que l'utilisateur est connecté
 * Si ce n'est pas le cas, redirection vers login.php
 * Si c'est le cas, redirection vers la page affichant les contacts (contact.php)
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date volée 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

session_start();

define("ROOT", ".");
require_once(ROOT . "/lib/security.php");

header("Location: " . ROOT . "/contact");
