<?php

/**
 * Constantes de la page login.php
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date volée 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

define("KEY_EMAIL", "email");
define("KEY_PASSWORD", "password");
