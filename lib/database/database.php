<?php

/**
 * Contient les fonctions permettant d'interagir avec la base de données
 * Dans un patron de conception ("design pattern") Modèle Vue Contrôleur, ce fichier constituerait le modèle.
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date année scolaire 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

require_once(ROOT . "/lib/database/config.php");

/**
 * Retourne la référence vers l'objet PHP Data Object (PDO) actuel.
 * 
 * @return PDO L'object PDO établissant le lien avec la base de données.
 */
function getPdoInstance()
{
    static $pdo = null;
    if ($pdo == null) {
        try {
            $pdo = new PDO("mysql:host=" . PDO_HOSTNAME . ";dbname=" . PDO_DB_NAME . ";charset=utf8mb4", PDO_DB_USER, PDO_DB_PASSWORD);

            /* Activation de l'affichage des messages d'erreur.
               Attention: à supprimer pour une mise en production
            */
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            /* Attention: en production les messages d'erreur interne ne doivent pas être affichés.
               Ils doivent être remplacés par les codes d'erreurs de la documentation utilisateur permettant d'ouvrir des tickets d'anomalie.
            */
            echo "<p>Erreur : " . $e->getMessage() . "<br>";
            echo "N° : " . $e->getCode() . "</p>";
            exit(1);
        }
    }
    return $pdo;
}

/**
 * Execute une requête SQL
 * 
 * @param PDOStatement $req contient la requête sql à exécuter.
 * @return None
 */
function execute($req)
{
    try {
        $req->execute();
    } catch (PDOException $e) {
        /* Attention: en production les messages d'erreur interne ne doivent pas être affichés.
           Ils doivent être remplacés par les codes d'erreurs de la documentation utilisateur permettant d'ouvrir des tickets d'anomalie.
        */
        echo "<p>Erreur : " . $e->getMessage() . "<br>";
        echo "N° : " . $e->getCode() . "</p>";
        exit(1);
    }
}

/**
 * Authentifie l'utilisateur
 * 
 * @param String $login Le nom d'utilisateur
 * @param String $password Le mot de passe en clair
 * @return Int|Boolean Retourne l'id de l'utilisateur si l'authentification est valide, Faux sinon.
 */
function login($login, $password)
{
    $pdo = getPdoInstance();
    $sql = "select id, password from user where login=:login";
    $req = $pdo->prepare($sql);
    $req->bindParam(":login", $login);
    execute($req);
    $res = $req->fetch(PDO::FETCH_ASSOC);
    if (!$res) return false;
    if (password_verify($password, $res["password"])) {
        return $res["id"];
    } else {
        return false;
    }
}

/**
 * Crée un contact pour l'utilisateur connecté
 * 
 * @param String $name Le prénom du contact
 * @param String $surname Le nom du contact
 * @param String $phone Le téléphone du contact
 * @param String $email L'adresse courriel du contact
 * @param Int $idUser L'identifiant de l'utilisateur connecté
 * @return Int Retourne l'id du nouveau contact, retour Faux sinon
 */
function createContact($name, $surname, $phone, $email, $idUser)
{
    $pdo = getPdoInstance();
    $sql = "insert into contact(name, surname, phone, email, idUser) value(:name, :surname, :phone, :email, :idUser)";
    $req = $pdo->prepare($sql);
    $req->bindParam(":name", $name);
    $req->bindParam(":surname", $surname);
    $req->bindParam(":phone", $phone);
    $req->bindParam(":email", $email);
    $req->bindParam(":idUser", $idUser);
    execute($req);
    return $pdo->lastInsertId();
}

/**
 * Retourne tous les contacts d'un utilisateur
 * 
 * @param Int $idUser l'identifiant de l'utilisateur
 * @return Array La liste des contacts de l'utilisateur
 */
function getContacts($idUser)
{
    $pdo = getPdoInstance();
    $sql = "select * from contact where idUser=:idUser";
    $req = $pdo->prepare($sql);
    $req->bindParam(":idUser", $idUser);
    execute($req);
    return $req->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * Retourne le contact dont l'identifiant est fourni en paramètre  
 * 
 * @param Int $id l'identifiant du contact à retourner
 * @return Array La liste des contacts
 */
function getContact($id)
{
    $pdo = getPdoInstance();
    $sql = "select * from contact where id=:id";
    $req = $pdo->prepare($sql);
    $req->bindParam(":id", $id);
    execute($req);
    return $req->fetch(PDO::FETCH_ASSOC);
}

/**
 * Suppression du contact dont l'identifiant est fourni en paramètre  
 * 
 * @param Int $id l'identifiant du contact à supprimer
 * @return None
 */
function deleteContact($id) {
    $pdo = getPdoInstance();
    $sql = "delete from contact where id=:id";
    $req = $pdo->prepare($sql);
    $req->bindParam(":id", $id);
    execute($req);
}

/**
 * Mise à jour d'un contact
 * 
 * @param String $name Le prénom du contact
 * @param String $surname Le nom du contact
 * @param String $phone Le téléphone du contact
 * @param String $email L'adresse courriel du contact
 * @param Int $id L'identifiant du contact
 * @return None
 */
function updateContact($name, $surname, $phone, $email, $id) {
    $pdo = getPdoInstance();
    $sql = "update contact set name=:name, surname=:surname, phone=:phone, email=:email where id=:id";
    $req = $pdo->prepare($sql);
    $req->bindParam(":name", $name);
    $req->bindParam(":surname", $surname);
    $req->bindParam(":phone", $phone);
    $req->bindParam(":email", $email);
    $req->bindParam(":id", $id);
    execute($req);
}