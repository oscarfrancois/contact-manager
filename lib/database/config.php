<?php

/**
 * Contient les informations de connexion à la base de données
 * 
 * Avertissement: ceci est un projet volontairement simplifié à des fins pédagogiques.
 * Pour un environnement de production, les informations de connexion sont à gérer séparemment
 * du code source (par exemple via les variables d'environnement du système de production).
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date année scolaire 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

define("PDO_HOSTNAME", "localhost");
define("PDO_DB_NAME", "contact");
define("PDO_DB_USER", "admin");
define("PDO_DB_PASSWORD", "Super");
