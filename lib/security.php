<?php

/**
 * Verifie que la session de l'utilisateur est valide.
 * Si ce n'est pas le cas, on redirige vers la page de login.
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date volée 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

if (!isset($_SESSION["idUser"])) {
    header("Location: " . ROOT . "/login.php");
}
