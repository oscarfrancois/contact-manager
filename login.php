<?php

/**
 * Page d'authentification de l'utilisateur.
 * Une fois l'utilisateur authentifié, redirige vers la page contact.php
 * 
 * @projet gestionnaire de contacts
 * @version 1.0.0
 * @date volée 2021/2022
 * @auteur oscar françois
 * @licence gpl v3: https://www.gnu.org/licenses/gpl-3.0.txt
 */

session_start();

define("ROOT", ".");

require_once(ROOT . "/const.php");
require_once(ROOT . "/lib/database/database.php");

// traitement du formulaire de login
if (isset($_POST[KEY_EMAIL]) && !empty($_POST[KEY_EMAIL])) {
  $email = filter_input(INPUT_POST, KEY_EMAIL, FILTER_SANITIZE_EMAIL);

  // vérification de la validité du format d'adresse courriel
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    echo '<p style="color:red">Courriel invalide, veuillez réessayer</p>';
  }
  else {
    // vérification du login + password
    $password = filter_input(INPUT_POST, KEY_PASSWORD, FILTER_SANITIZE_SPECIAL_CHARS);
    $idUser = login($email, $password);
    if ($idUser) {
      $_SESSION['idUser'] = $idUser;
      header("Location: " . ROOT . "/contact");
    } else {
      echo '<p style="color:red">Courriel et/ou mot de passe invalide, veuillez réessayer</p>';
    }
  }
}

// affichage initial du formulaire de login
?>
<!DOCTYPE html>
<html>
  <body>
    <form method="post" action="">
      <!-- l'adresse courriel est sticky -->
      Login: <input type="email" name="<?= KEY_EMAIL ?>" value="<?= (isset($email) ? $email : '') ?>" required autofocus>
      Password: <input type="password" name="<?= KEY_PASSWORD ?>">
      <input type="submit" name="submit" value="Login" />
    </form>
  </body>
</html>